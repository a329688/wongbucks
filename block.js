const SHA256 = require('./node_modules/crypto-js/sha256.js');

class Block{
//componets:
//index
//informacion
//previous hash = value of previous block in chain
  constructor(index, data, previousHash=''){
    this.index = index;
    this.data = data;
    this.previousHash = previousHash;
    this.date = new Date();
    this.nonce = 0;
    this.hash = this.createdHash();
  }

  createdHash(){
    return SHA256(`${this.index}${this.data}${this.date}${this.nonce}`);
  }
  mine(difficulty){
    while(!String(this.hash).startsWith(difficulty)){
      this.nonce++;
      this.hash = this.createdHash();
    }
  }
}

//start with 0 in hash
module.exports = Block;
